@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1KR1174 {
    bda:G1KR1174  a         adm:AdminData ;
        adm:adminAbout      bdr:G1KR1174 ;
        adm:facetIndex      10 ;
        adm:gitPath         "94/G1KR1174.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1KR1174 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG27E5AE8AB63F3788 , bda:LG356576951127140C , bda:LG5BF21351BD72C50D , bda:LGIM5E940A373A , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542125205:273" ;
        adm:place_id_lex    "542125205:273" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG27E5AE8AB63F3788
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-16T12:14:42.403000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added tibetan name & event & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG356576951127140C
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-16T12:27:02.593000+00:00"^^xsd:dateTime ;
        adm:logMessage      "update list of names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG5BF21351BD72C50D
        a                   adm:UpdateData ;
        adm:logDate         "2013-09-25T15:56:17.492000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added Gis coord & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM5E940A373A
        a                   adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created by monastery import"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EV1EDDCE89C18924A1
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:eventWhen       "1413"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G1KR1174  a         bdo:Place ;
        bf:identifiedBy     bdr:IDAC3D9F8122B0470F ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:note            bdr:NT955B29EC0F3BDB2D , bdr:NTB1A57F3FCFCF7132 ;
        bdo:placeEvent      bdr:EV1EDDCE89C18924A1 ;
        bdo:placeGonpaPerEcumen  "5.08" ;
        bdo:placeLat        31.585527 ;
        bdo:placeLocatedIn  bdr:G2188 ;
        bdo:placeLong       95.195849 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "pulai si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "phug leb dgon/"@bo-x-ewts , "普来寺"@zh-hans .
    
    bdr:IDAC3D9F8122B0470F
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1KR1174" .
    
    bdr:NT955B29EC0F3BDB2D
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 180-181" ;
        bdo:noteSource      bdr:MW25095 ;
        bdo:noteText        "སེར་ཚ་ཤང་འབུ་ཚབ་གྲོང་ཚོར་གནས། ༡༤༡༣ལོར་ཁྱུང་པོ་འབུམ་ཆེན་གྲགས་པས་ཕྱག་བཏབ། ཁྱུང་པོའི་གདན་སའི་བྱེ་བྲག་ཅིག་ཡིན་ལ་བོན་དགོན་རྙིང་མའི་ཁོངས་སུ་གཏོགས། ད་ལྟའང་ཆ་རྐྱེན་བཟང་ཙམ་ཡོད་པ་སོགས་གསལ།"@bo .
    
    bdr:NTB1A57F3FCFCF7132
        a                   bdo:Note ;
        bdo:noteText        "Founded by Khyung po ’Bum chen. One of the centres of the Khyung lineage.\n\n1 lama -  21 monks"@en .
}

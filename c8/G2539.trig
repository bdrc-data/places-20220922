@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G2539 {
    bda:G2539  a            adm:AdminData ;
        adm:adminAbout      bdr:G2539 ;
        adm:facetIndex      14 ;
        adm:gitPath         "c8/G2539.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G2539 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG7A562A646DC81976 , bda:LG84D9D26491FF6D14 , bda:LGC26344233A30DE5F , bda:LGDE7FACAA7A40DB00 , bda:LGED48FB6C5FF5A707 , bda:LGF30E458A1C7B8AAC , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632224200:549" ;
        adm:place_id_lex    "632224200:421" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG7A562A646DC81976
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG84D9D26491FF6D14
        a                   adm:UpdateData ;
        adm:logDate         "2009-12-22T10:38:19.070000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGC26344233A30DE5F
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-18T14:01:33.773000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names & added note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGDE7FACAA7A40DB00
        a                   adm:UpdateData ;
        adm:logDate         "2010-10-20T12:13:57.107000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00002 .
    
    bda:LGED48FB6C5FF5A707
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-18T14:21:37.475000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cleanup location info"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGF30E458A1C7B8AAC
        a                   adm:UpdateData ;
        adm:logDate         "2009-12-21T16:30:31.339000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV04A81D791D486224
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1915"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G2539  a            bdo:Place ;
        bf:identifiedBy     bdr:IDBAF5884F5C4CB2BE ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT4C04AE6DF8A82664 , bdr:NT611D160F183FE604 , bdr:NTB0610E8B1C9FFB86 ;
        bdo:placeEvent      bdr:EV04A81D791D486224 ;
        bdo:placeGonpaPerEcumen  "4.05" ;
        bdo:placeLat        37.53525 ;
        bdo:placeLocatedIn  bdr:G2315 ;
        bdo:placeLong       100.13000 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "dga' ldan chos 'phel gling /"@bo-x-ewts , "rkang tsha dgon/"@bo-x-ewts , "rkang tsha dgon chen dga' ldan chos 'phel gling /"@bo-x-ewts , "rkang tsha dgon chen mi 'gyur dga' ldan chos 'phel gling /"@bo-x-ewts , "刚察大寺"@zh-hans , "gangca si"@zh-latn-pinyin-x-ndia , "gangcha dasi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "rkang tsha dgon chen/"@bo-x-ewts , "Kangtsa Gon Ganden Chopeling"@en , "岗察寺"@zh-hans .
    
    bdr:IDBAF5884F5C4CB2BE
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G2539" .
    
    bdr:NT4C04AE6DF8A82664
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 129" ;
        bdo:noteSource      bdr:MW1PD95675 .
    
    bdr:NT611D160F183FE604
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 110" ;
        bdo:noteSource      bdr:MW20197 ;
        bdo:noteText        "dge lugs pa monastery founded at the end of the 15 cycle, i.e. at the beginning of the 20th century, by rkang tsha stong dpon chen mo gnas brtan dbang rgyal\nlargely restored\nnow there are a total of about 70 monks"@en .
    
    bdr:NTB0610E8B1C9FFB86
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v.4, p. 415" ;
        bdo:noteSource      bdr:MW2CZ7959 ;
        bdo:noteText        "འདི་ནི་རྐང་ཚ་ཕྱོགས་ཀྱི་དགོན་སྡེ་ཆེ་ཤོས་ཡིན་པ་སོགས་ལོ་རྒྱུས་གནད་བསྡུས་གསལ།"@bo .
}

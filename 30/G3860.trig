@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3860 {
    bda:G3860  a            adm:AdminData ;
        adm:adminAbout      bdr:G3860 ;
        adm:facetIndex      11 ;
        adm:gitPath         "30/G3860.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3860 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG113DB26A01BC2916 , bda:LG5E0347D36E21A3A9 , bda:LG9FDBAEFD8B6F9DA1 , bda:LGE5C49B3244B4D6FF , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "513337100:423" ;
        adm:place_id_lex    "513337100:423" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG113DB26A01BC2916
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-12T16:32:26.723000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & events"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG5E0347D36E21A3A9
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG9FDBAEFD8B6F9DA1
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-12T12:36:38.048000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGE5C49B3244B4D6FF
        a                   adm:UpdateData ;
        adm:logDate         "2014-06-24T09:22:46.954000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV3AEF0FC35B7CC91B
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionKagyu ;
        bdo:eventWhen       "1100"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV945B7ACE3FC769A5
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "14XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3860  a            bdo:Place ;
        bf:identifiedBy     bdr:ID9A7E5AEC623291C3 ;
        bdo:associatedTradition  bdr:TraditionGeluk , bdr:TraditionKagyu ;
        bdo:note            bdr:NT29BE93767FDBF213 ;
        bdo:placeEvent      bdr:EV3AEF0FC35B7CC91B , bdr:EV945B7ACE3FC769A5 ;
        bdo:placeGonpaPerEcumen  "5.76" ;
        bdo:placeLat        29.03333 ;
        bdo:placeLocatedIn  bdr:G1121 , bdr:G2306 ;
        bdo:placeLong       100.28333 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "oM dgon ltag ma (bka' brgyud dgon rnying)"@bo-x-ewts , "oM dgon yang ma (bka' brgyud dgon rnying)"@bo-x-ewts , "yang ston dgon/"@bo-x-ewts , "yangdeng si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "yang steng dgon/"@bo-x-ewts , "洋登寺"@zh-hans .
    
    bdr:ID9A7E5AEC623291C3
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3860" .
    
    bdr:NT29BE93767FDBF213
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 3, pp. 510-518" ;
        bdo:noteSource      bdr:MW19997 ;
        bdo:noteText        "dge lugs monastery in 'dab pa rdzong"@en .
}

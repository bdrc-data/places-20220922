@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1PD96031 {
    bda:G1PD96031  a        adm:AdminData ;
        adm:adminAbout      bdr:G1PD96031 ;
        adm:facetIndex      12 ;
        adm:gitPath         "47/G1PD96031.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1PD96031 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG541DBD1F05CEA1C5 , bda:LG5CEEA4106D185DF6 , bda:LG774D57C31A16167A , bda:LG807F721843282FB2 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632122103:61" ;
        adm:place_id_lex    "632122103:10" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG541DBD1F05CEA1C5
        a                   adm:UpdateData ;
        adm:logDate         "2010-01-13T14:33:18.331000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00003 .
    
    bda:LG5CEEA4106D185DF6
        a                   adm:UpdateData ;
        adm:logDate         "2009-12-08T12:11:50.731000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LG774D57C31A16167A
        a                   adm:UpdateData ;
        adm:logDate         "2009-11-30T17:21:11.740000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LG807F721843282FB2
        a                   adm:InitialDataCreation ;
        adm:logDate         "2009-11-30T15:03:29.732000+00:00"^^xsd:dateTime ;
        adm:logMessage      "གསར་དུ་བཅུག"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV00C3875914C31927
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1600"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV06B6E92E8F4ED931
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1100"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:note            bdr:NTDFAADA1F84E79794 .
    
    bdr:G1PD96031  a        bdo:Place ;
        bf:identifiedBy     bdr:ID3CB40743B705CCF8 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NTDC7465F45863F4EA ;
        bdo:placeEvent      bdr:EV00C3875914C31927 , bdr:EV06B6E92E8F4ED931 ;
        bdo:placeGonpaPerEcumen  "101.22" ;
        bdo:placeLat        35.88000 ;
        bdo:placeLocatedIn  bdr:G1PD95976 ;
        bdo:placeLong       102.82000 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "Chinkya"@bo-x-phon-en , "祭骨寺"@zh-hans , "秦家寺"@zh-hans , "qinjia si"@zh-latn-pinyin-x-ndia , "qin jia si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "chin kya zi/"@bo-x-ewts , "秦家寺"@zh-hans .
    
    bdr:ID3CB40743B705CCF8
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1PD96031" .
    
    bdr:NTDC7465F45863F4EA
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. ༤༡" ;
        bdo:noteSource      bdr:MW1PD95675 ;
        bdo:noteText        "སུང་རྒྱལ་རབས་ཀྱི་སྐབས་སུ་ཕྱག་བཏབ།"@bo .
    
    bdr:NTDFAADA1F84E79794
        a                   bdo:Note ;
        bdo:noteText        "no-sect"@en .
}

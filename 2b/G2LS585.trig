@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G2LS585 {
    bda:G2LS585  a          adm:AdminData ;
        adm:adminAbout      bdr:G2LS585 ;
        adm:facetIndex      10 ;
        adm:gitPath         "2b/G2LS585.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G2LS585 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG479159AF4225E98B , bda:LG87E756790315703D , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542522203:12" ;
        adm:place_id_lex    "542522203:12" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG479159AF4225E98B
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-03-29T15:55:44.315000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00019 .
    
    bda:LG87E756790315703D
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-19T11:06:19.987000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV802CE7FC831F094B
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1450"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVFACB6720986DFEBE
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1050"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:note            bdr:NT6B8BC3EF81B8CE45 .
    
    bdr:G2LS585  a          bdo:Place ;
        bf:identifiedBy     bdr:ID57E84C182C3B5898 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT5B6453425AC87A2D ;
        bdo:placeEvent      bdr:EV802CE7FC831F094B , bdr:EVFACB6720986DFEBE ;
        bdo:placeGonpaPerEcumen  "2.93" ;
        bdo:placeLat        31.50000 ;
        bdo:placeLocatedIn  bdr:G2152 ;
        bdo:placeLong       79.00000 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "Teng Phagspa"@bo-x-phon-en ;
        skos:prefLabel      "steng 'phags mthog ba don ldan/"@bo-x-ewts .
    
    bdr:ID57E84C182C3B5898
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G2LS585" .
    
    bdr:NT5B6453425AC87A2D
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 4, p. 85" ;
        bdo:noteSource      bdr:MW23847 ;
        bdo:noteText        "According to oral history, it is said to be founded by Lochen rinchen zangpo and expaded by Namkha Tashi. Became branch monastery of Tholing in 18th century. \n\nSeven brothers of phagpas are : one choku phag pa, two garzha phagpa, three gungthang phagpa, four droshod phagpa, five teng phagpa, six khyunglung phagpa, seven nyungti phagpa"@en .
    
    bdr:NT6B8BC3EF81B8CE45
        a                   bdo:Note ;
        bdo:noteText        "gsang sngags gsar ma/"@bo-x-ewts .
}

@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3953 {
    bda:G3953  a            adm:AdminData ;
        adm:adminAbout      bdr:G3953 ;
        adm:facetIndex      10 ;
        adm:gitPath         "6c/G3953.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3953 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG9C2E72436694290F , bda:LG9DC32E36EEA39A77 , bda:LGA4753F04176CFB5D , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "513332219:385" ;
        adm:place_id_lex    "513332219:385" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG9C2E72436694290F
        a                   adm:UpdateData ;
        adm:logDate         "2012-06-15T11:24:31.184000+00:00"^^xsd:dateTime ;
        adm:logMessage      "edited title brackets"@en ;
        adm:logWho          bdu:U00020 .
    
    bda:LG9DC32E36EEA39A77
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-16T11:45:53.593000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGA4753F04176CFB5D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV2CB5B7918B845997
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1182"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P8326 .
    
    bdr:EV5D02D0E99BB182AD
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1182"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3953  a            bdo:Place ;
        bf:identifiedBy     bdr:IDD8623694EF1A2319 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT3BEE4198015C3759 ;
        bdo:placeEvent      bdr:EV2CB5B7918B845997 , bdr:EV5D02D0E99BB182AD ;
        bdo:placeGonpaPerEcumen  "3.24" ;
        bdo:placeLat        32.58333 ;
        bdo:placeLocatedIn  bdr:G2301 ;
        bdo:placeLong       98.88333 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "e cha si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "mgo tshwa dgon/"@bo-x-ewts , "俄查寺"@zh-hans .
    
    bdr:IDD8623694EF1A2319
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3953" .
    
    bdr:NT3BEE4198015C3759
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 2, pp. 235-237" ;
        bdo:noteSource      bdr:MW19997 ;
        bdo:noteText        "rnying ma monastery in ser shul rdzong belonging to the byang gter\nvery ancient first founded by mgo tsha kun dga' byams pa rgyal mtshan in about 1182\nthe monastery is relatively large and enjoys the patronage of 565 families of the wa shul tribe"@en .
}

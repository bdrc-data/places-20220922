@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1898 {
    bda:G1898  a            adm:AdminData ;
        adm:adminAbout      bdr:G1898 ;
        adm:facetIndex      10 ;
        adm:gitPath         "20/G1898.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1898 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG35EADE302D0BBF52 , bda:LG962DE623EEEAADFF , bda:LGFF2515F11BAFEC92 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632521206:431" ;
        adm:place_id_lex    "632521206:534" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG35EADE302D0BBF52
        a                   adm:UpdateData ;
        adm:logDate         "2010-10-07T16:09:27.916000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00002 .
    
    bda:LG962DE623EEEAADFF
        a                   adm:UpdateData ;
        adm:logDate         "2013-09-11T10:29:49.957000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGFF2515F11BAFEC92
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV0438EF1F021866CC
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1984"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV8A56DC29E1F89FC7
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1980"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P6770 .
    
    bdr:G1898  a            bdo:Place ;
        bf:identifiedBy     bdr:IDF7F5618887F1F95D ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT50E7CAE3E79CA225 ;
        bdo:placeEvent      bdr:EV0438EF1F021866CC , bdr:EV8A56DC29E1F89FC7 ;
        bdo:placeGonpaPerEcumen  "2.43" ;
        bdo:placeLat        36.49279 ;
        bdo:placeLocatedIn  bdr:G1698 ;
        bdo:placeLong       100.67160 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "ngo mtshar thar 'dren gling /"@bo-x-ewts , "rgya ye dgon ngo mtshar thar 'dren gling /"@bo-x-ewts , "甲乙俄察塔占林"@zh-hans , "jiayi eca tazhenlin"@zh-latn-pinyin-x-ndia , "jiayi si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "rgya ye dgon/"@bo-x-ewts , "Gyaye Gon Ngotsar Tardrenling"@en , "甲乙寺"@zh-hans .
    
    bdr:IDF7F5618887F1F95D
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1898" .
    
    bdr:NT50E7CAE3E79CA225
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 422" ;
        bdo:noteSource      bdr:MW20214 ;
        bdo:noteText        "monastery in gung ho rdzong, mtsho lho bod rigs rang skyong khul, located 28 km. to the north of chab cha grong rdal\nand about 27 km. to the northwest of rigs smon zhang on the banks of the kokonor lake\nfounded in 1984 by the gung ri mkha' 'gro ma dam tshig sgrol ma in accordance with the wishes of the 7th panchen lama"@en .
}

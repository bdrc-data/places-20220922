@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3950 {
    bda:G3950  a            adm:AdminData ;
        adm:adminAbout      bdr:G3950 ;
        adm:facetIndex      11 ;
        adm:gitPath         "3a/G3950.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3950 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG28EFC603D61E26D6 , bda:LG43B27AE6B59E98FD , bda:LGCE189178344E8A58 , bda:LGFB1CFFC39B26B83C , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "513332207:365" ;
        adm:place_id_lex    "513332207:365" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG28EFC603D61E26D6
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG43B27AE6B59E98FD
        a                   adm:UpdateData ;
        adm:logDate         "2010-08-07T09:14:00.664000+00:00"^^xsd:dateTime ;
        adm:logMessage      "reviewed"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGCE189178344E8A58
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-16T11:05:37.165000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGFB1CFFC39B26B83C
        a                   adm:UpdateData ;
        adm:logDate         "2010-08-07T09:26:10.946000+00:00"^^xsd:dateTime ;
        adm:logMessage      "note added from khams sde dge rgyal po'i srid don lo rgyus"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV9A1CB9F2BD1EB2F9
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1675"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3950  a            bdo:Place ;
        bf:identifiedBy     bdr:IDD540B6D2A67DE970 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NTA1226A917EF0FDD3 , bdr:NTF5CA56E68658F9B6 ;
        bdo:placeEvent      bdr:EV9A1CB9F2BD1EB2F9 ;
        bdo:placeGonpaPerEcumen  "0.93" ;
        bdo:placeLat        33.38334 ;
        bdo:placeLocatedIn  bdr:G2301 ;
        bdo:placeLong       97.80000 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "baruo si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "ba rod dgon/"@bo-x-ewts , "巴若寺"@zh-hans .
    
    bdr:IDD540B6D2A67DE970
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3950" .
    
    bdr:NTA1226A917EF0FDD3
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 2, pp. 226-230" ;
        bdo:noteSource      bdr:MW19997 ;
        bdo:noteText        "dge lugs monastery in ser shul rdzong"@en .
    
    bdr:NTF5CA56E68658F9B6
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 152" ;
        bdo:noteSource      bdr:MW1KG5539 ;
        bdo:noteText        "dge lugs monastery; founded by jo bo skal bzang dge legs rnam rgyal; moved from shag khog in about 1870; the chab mdo lcags rwa rin po che ngag dbang 'phrin las performed the rites of sa 'dl and rab gnas; formerly had about 300 monks; now there are about 50 monks"@en .
}

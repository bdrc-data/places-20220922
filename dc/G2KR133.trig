@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G2KR133 {
    bda:G2KR133  a          adm:AdminData ;
        adm:adminAbout      bdr:G2KR133 ;
        adm:facetIndex      3 ;
        adm:gitPath         "dc/G2KR133.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G2KR133 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG0BDSGLPZU0FYWSHW , bda:LGIM335E72436E ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSGLPZU0FYWSHW
        a                   adm:UpdateData ;
        adm:logAgent        "domm-import/domm-import.py" ;
        adm:logDate         "2024-06-26T09:00:04.708062"^^xsd:dateTime ;
        adm:logMessage      "import data from DOMM"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM335E72436E
        a                   adm:UpdateData ;
        adm:logAgent        "mongol import" ;
        adm:logDate         "2013-12-03T23:23:40.962000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created by mongol import"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EVE28485DF2F39A69A
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1721"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G2KR133  a          bdo:Place ;
        rdfs:seeAlso        "https://mongoliantemples.org/mn/component/domm/1264?view=oldtempleen"^^xsd:anyURI , "https://mongoliantemples.org/mn/component/domm/245?view=oldtemplemn"^^xsd:anyURI ;
        bf:identifiedBy     bdr:IDG2KR133_001 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NTG2KR133_NP1 , bdr:NTG2KR133_P ;
        bdo:placeAccuracy   "GIS coordinates have been truncated at the request of DOMM in order to preserve potential remains of the monastery."@en ;
        bdo:placeEvent      bdr:EVE28485DF2F39A69A ;
        bdo:placeLat        "46.30" , 46.30935 ;
        bdo:placeLocatedIn  bdr:G584 ;
        bdo:placeLong       "93.90" , 93.90088333 ;
        bdo:placeType       bdr:PT0037 , bdr:PT0059 ;
        skos:altLabel       "Ганданравжаачойнхорлин"@mn-Cyrl , "Дайчин вангийн хүрээ"@mn-Cyrl , "Жалчин бэйсийн хүрээ"@mn-Cyrl , "Усан зүйлийн хүрээ"@mn-Cyrl , "Jaltsan beisiin khuree"@mn-x-trans , "usun juyil-un kuriy-e"@mn-x-trans ;
        skos:prefLabel      "*dga' ldan rab rgyas chos 'khor gling /"@bo-x-ewts , "Ганданравжаачойнхорлин"@mn-Cyrl , "Зүйлийн хүрээ"@mn-Cyrl , "Usan zuiliin khuree"@mn-x-trans .
    
    bdr:IDG2KR133_001
        a                   bdr:DommId ;
        rdf:value           "ГАТХ 108" .
    
    bdr:NTG2KR133_NP1
        a                   bdo:Note ;
        bdo:noteText        "Marked as a printery by Purev. Foundation date given as 1763 by Purev."@en .
    
    bdr:NTG2KR133_P  a      bdo:Note ;
        bdo:noteText        "This record has been updated with data from the “Documentation of Mongolian Monasteries” project with due authorization. For more information on this monastery, including oral history, historical records and pictures, see the links above."@en .
}

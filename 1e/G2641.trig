@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G2641 {
    bda:G2641  a            adm:AdminData ;
        adm:adminAbout      bdr:G2641 ;
        adm:facetIndex      10 ;
        adm:gitPath         "1e/G2641.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G2641 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG2372A8C1A432833C , bda:LG84020508334A57B5 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632725205:165" ;
        adm:place_id_lex    "632725205:165" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG2372A8C1A432833C
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-07T13:54:34.077000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location & arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG84020508334A57B5
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV37B1C4EFDC6FF040
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionKarmaKagyu ;
        bdo:eventWhen       "1673"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVB14DA4465B05C664
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionShangpaKagyu ;
        bdo:eventWhen       "1400"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G2641  a            bdo:Place ;
        bf:identifiedBy     bdr:ID2ACCDF00B2DDCF42 ;
        bdo:associatedTradition  bdr:TraditionKarmaKagyu , bdr:TraditionShangpaKagyu ;
        bdo:note            bdr:NT7CD1BF6A2B602280 , bdr:NTF001A689CD0F463B ;
        bdo:placeEvent      bdr:EV37B1C4EFDC6FF040 , bdr:EVB14DA4465B05C664 ;
        bdo:placeGonpaPerEcumen  "11.11" ;
        bdo:placeLat        32.31750 ;
        bdo:placeLocatedIn  bdr:G1467 ;
        bdo:placeLong       95.60130 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "干勃寺"@zh-hans , "ganbo si"@zh-latn-pinyin-x-ndia , "gongbao si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "mgon po dgon/"@bo-x-ewts , "公保寺"@zh-hans .
    
    bdr:ID2ACCDF00B2DDCF42
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G2641" .
    
    bdr:NT7CD1BF6A2B602280
        a                   bdo:Note ;
        bdo:noteText        "used to be Shangpa Kagyu, changed c. 1650-1673"@en .
    
    bdr:NTF001A689CD0F463B
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, p. 47" ;
        bdo:noteSource      bdr:MW18134 ;
        bdo:noteText        "karma bka' brgyud pa monastery in yus hru'u prefecture, qinghai province"@en .
}

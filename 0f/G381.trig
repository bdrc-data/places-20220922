@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G381 {
    bda:G381  a             adm:AdminData ;
        adm:adminAbout      bdr:G381 ;
        adm:facetIndex      17 ;
        adm:gitPath         "0f/G381.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G381 ;
        adm:logEntry        bda:LG0B6EAB148D9AF106 , bda:LG0BDSG2MOZVR13LOA , bda:LG1AC3EABB7995FB6F , bda:LG60306C6D55326A35 , bda:LG7C8A38052ABE42FD , bda:LG8433A2865C720C65 , bda:LGBFEBEB48D5D25482 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542327204:542327105" ;
        adm:place_id_lex    "542327204:542327105" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0B6EAB148D9AF106
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1AC3EABB7995FB6F
        a                   adm:UpdateData ;
        adm:logDate         "2014-07-11T16:23:38.573000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added description"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG60306C6D55326A35
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-11T11:07:12.644000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & events & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG7C8A38052ABE42FD
        a                   adm:UpdateData ;
        adm:logDate         "2012-07-05T12:10:23.244000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added name & location"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG8433A2865C720C65
        a                   adm:UpdateData ;
        adm:logDate         "2013-06-13T14:40:03.655000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added event"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGBFEBEB48D5D25482
        a                   adm:UpdateData ;
        adm:logDate         "2012-03-21T14:57:39.506000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added 2nd note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV6F5EE7BC6E794CD8
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:eventWhen       "1450"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV8AB19E303CED8DF1
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionShangpaKagyu ;
        bdo:eventWhen       "1426"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVB83FACD137CFBEBA
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionShangpaKagyu ;
        bdo:eventWhen       "1386"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G381  a             bdo:Place ;
        bf:identifiedBy     bdr:ID0A57A02EE2F61B0C ;
        bdo:associatedTradition  bdr:TraditionNyingma , bdr:TraditionShangpaKagyu ;
        bdo:contentLocationStatement  "Coordinates: latitude 29°11,535 longitude 86°36,954 Altitude: 4184(m) (sheehy_tibet_coordinates_2014)" ;
        bdo:note            bdr:NT5021B732EE89E643 , bdr:NT609461D640019842 , bdr:NTE00EAFCD512DDFF9 ;
        bdo:placeEvent      bdr:EV6F5EE7BC6E794CD8 , bdr:EV8AB19E303CED8DF1 , bdr:EVB83FACD137CFBEBA ;
        bdo:placeGonpaPerEcumen  "4.79" ;
        bdo:placeLat        29.18333 ;
        bdo:placeLocatedIn  bdr:G2162 ;
        bdo:placeLong       86.61667 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "dpal ri bo che dgon/"@bo-x-ewts , "dpal ri bo che dgon e waM dga' 'khyil/"@bo-x-ewts , "e waM dga' 'khyil/"@bo-x-ewts , "gcung ri bo che dgon/"@bo-x-ewts , "Chung Riwoche"@bo-x-phon-en , "riwuqi si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "cung ri bo che/"@bo-x-ewts , "日吾其寺"@zh-hans .
    
    bdr:ID0A57A02EE2F61B0C
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G381" .
    
    bdr:NT5021B732EE89E643
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 377-382" ;
        bdo:noteSource      bdr:MW21509 ;
        bdo:noteText        "དགོན་འདིའི་ལོ་རྒྱུས་གསལ།"@bo .
    
    bdr:NT609461D640019842
        a                   bdo:Note ;
        bdo:noteText        "shangs pa bka' brgyud monastery in gtsang in the tibetan autonomous region; the seat of the lcags zam pa incarnation lineage, reembodiments of thang stong rgyal po"@en .
    
    bdr:NTE00EAFCD512DDFF9
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 18, p. 324" ;
        bdo:noteSource      bdr:MW21807 ;
        bdo:noteText        "གཅུང་རི་བོ་ཆེ་ནི་ཐང་སྟོང་པས་བཏབ། ཕྱིས་ཡོལ་མོ་གཏེར་སྟོན་བསྟན་འཛིན་ནོར་བུ་ལ་ཕྱག་ཏུ་བཞེས་ནས། དེའི་གཅུང་སྒོམ་སྨྱོན་ཕྱག་རྡོར་ནོར་བུའི་སྐྱེ་སྤྲུལ་རིམ་བྱོན་གྱིས་གདན་ས་མཛད། ཅེས་འཇམ་དབྱངས་མཁྱེན་བརྩེ་དབང་པོའི་གསུང་འབུམ་འདིར་ཞལ་གསལ་བར་གསུངས་སོ།"@bo .
}

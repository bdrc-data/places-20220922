@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1KR1630 {
    bda:G1KR1630  a         adm:AdminData ;
        adm:adminAbout      bdr:G1KR1630 ;
        adm:facetIndex      12 ;
        adm:gitPath         "5f/G1KR1630.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1KR1630 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG3023B570BD0C9447 , bda:LGD1EE56B22B1AF9FB , bda:LGEE9A3C8073EFF48C , bda:LGIM5E940A373A , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542333201:542333101" ;
        adm:place_id_lex    "542333201:542333101" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3023B570BD0C9447
        a                   adm:UpdateData ;
        adm:logDate         "2015-03-17T09:43:22.267000+00:00"^^xsd:dateTime ;
        adm:logMessage      "corrected language settings"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGD1EE56B22B1AF9FB
        a                   adm:UpdateData ;
        adm:logDate         "2014-08-08T16:42:15.026000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location, names & 2 notes in tibetan"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGEE9A3C8073EFF48C
        a                   adm:UpdateData ;
        adm:logDate         "2014-07-16T15:09:07.698000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added name"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM5E940A373A
        a                   adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created by monastery import"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EV0C43C2822FA6EBDC
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionKagyu ;
        bdo:eventWhen       "1050"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV9AC6A0AD8FD35822
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionRime ;
        bdo:eventWhen       "0650"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVA27CB6FFE47E51F6
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1650"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G1KR1630  a         bdo:Place ;
        bf:identifiedBy     bdr:ID7344EE80DF145DC1 ;
        bdo:associatedTradition  bdr:TraditionGeluk , bdr:TraditionKagyu , bdr:TraditionRime ;
        bdo:note            bdr:NT3E7E014269D276D3 , bdr:NTF0BFEBFB8092049F ;
        bdo:placeEvent      bdr:EV0C43C2822FA6EBDC , bdr:EV9AC6A0AD8FD35822 , bdr:EVA27CB6FFE47E51F6 ;
        bdo:placeGonpaPerEcumen  "1.16" ;
        bdo:placeLat        29.65000 ;
        bdo:placeLocatedIn  bdr:G2159 , bdr:G3CN366 ;
        bdo:placeLong       84.16666 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "byang pra dun brtse/"@bo-x-ewts , "pra dan tse/"@bo-x-ewts , "skra bdun rtse/"@bo-x-ewts , "Traduntse"@bo-x-phon-en , "诈顿拉康"@zh-hans , "zhadun lakang"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "pra dun brtse/"@bo-x-ewts , "江扎东哲寺"@zh-hans .
    
    bdr:ID7344EE80DF145DC1
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1KR1630" .
    
    bdr:NT3E7E014269D276D3
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 123" ;
        bdo:noteSource      bdr:MW1PD96069 ;
        bdo:noteText        "ཡང་འདུལ་བཞི་ལས། པུས་མོ་གཡོན་པ་ལ་བྱང་རུས་སྦལ་གྱི་དཔྲལ་སྟེང་དུ་པྲ་དན་ཙེའི་(བྱང་པྲ་དུན་བརྩེ་)ལྷ་ཁང་བཏབ་སྟེ།\" ཞེས་གསལ།"@bo .
    
    bdr:NTF0BFEBFB8092049F
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 115-116" ;
        bdo:noteSource      bdr:MW20831 ;
        bdo:noteText        "\"བྱང་དུ་སྦལ་པ་ནག་པོའི་མཆུ་སྟེངས་སུ་སྤྲ་དུན་ཙེ། དེའི་འཕྱོང་དུ་གཉེན་གསལ་གྱི་ལྷ་ཁང་བཞེངས་སོ། \nདེ་(སོགས་)ནི་ཡང་འདུལ་བཞི་ལ་འཕྱོང་གི་བཏགས་པ་བཞི་སྟེ་བརྒྱད་དོ།\" ཞེས་གསལ།"@bo .
}

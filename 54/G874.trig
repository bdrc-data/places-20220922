@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G874 {
    bda:G874  a             adm:AdminData ;
        adm:adminAbout      bdr:G874 ;
        adm:facetIndex      13 ;
        adm:gitPath         "54/G874.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G874 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG0E548A50A8EECD8A , bda:LG1F1523259399AD1A , bda:LG46F4E28DF5E17FF8 , bda:LG7C6F1DCA519C6F1E , bda:LG90589DE98B50AFB8 , bda:LGFF010F0D00492DBE , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632123217:87" ;
        adm:place_id_lex    "632123217:127" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0E548A50A8EECD8A
        a                   adm:UpdateData ;
        adm:logDate         "2009-12-09T11:51:24.649000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LG1F1523259399AD1A
        a                   adm:UpdateData ;
        adm:logDate         "2013-05-20T11:45:33.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arrange dnames"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG46F4E28DF5E17FF8
        a                   adm:UpdateData ;
        adm:logDate         "2009-12-01T14:59:32.675000+00:00"^^xsd:dateTime ;
        adm:logMessage      "gsar du bcug"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LG7C6F1DCA519C6F1E
        a                   adm:UpdateData ;
        adm:logDate         "2009-12-01T15:08:29.132000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LG90589DE98B50AFB8
        a                   adm:UpdateData ;
        adm:logDate         "2010-01-13T14:33:25.084000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00003 .
    
    bda:LGFF010F0D00492DBE
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV2A207BFEE1EA8C74
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1619"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVA3E9B99C91AAA4B3
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "13XX/14XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P10316 .
    
    bdr:G874  a             bdo:Place ;
        bf:identifiedBy     bdr:IDE9B3EFFF86C8A2AA ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT649CCE4A0B584228 ;
        bdo:placeEvent      bdr:EV2A207BFEE1EA8C74 , bdr:EVA3E9B99C91AAA4B3 ;
        bdo:placeGonpaPerEcumen  "10.39" ;
        bdo:placeLat        36.29263 ;
        bdo:placeLocatedIn  bdr:G1PD5303 ;
        bdo:placeLong       102.22916 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "gro tshang dgon bkra shis lhun po/"@bo-x-ewts , "yaocaotai si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "gro tshang dgon/"@bo-x-ewts , "药草台寺"@zh-hans .
    
    bdr:IDE9B3EFFF86C8A2AA
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G874" .
    
    bdr:NT649CCE4A0B584228
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 52" ;
        bdo:noteSource      bdr:MW1PD95675 .
}

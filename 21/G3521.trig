@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3521 {
    bda:G3521  a            adm:AdminData ;
        adm:adminAbout      bdr:G3521 ;
        adm:facetIndex      15 ;
        adm:gitPath         "21/G3521.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3521 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG133009724D3E6353 , bda:LG792923432FCFB272 , bda:LG828EE0E12C77E577 , bda:LGE55ECC145F4AB2D0 , bda:LGF69B5087DC2D4B3F , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542222201:542222122" ;
        adm:place_id_lex    "542222201:542222122" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG133009724D3E6353
        a                   adm:UpdateData ;
        adm:logDate         "2022-06-01T13:14:38.003000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added gra nang rgyal gling tshogs pa/"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG792923432FCFB272
        a                   adm:UpdateData ;
        adm:logDate         "2014-06-18T15:44:17.324000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added name & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG828EE0E12C77E577
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGE55ECC145F4AB2D0
        a                   adm:UpdateData ;
        adm:logDate         "2012-05-11T12:54:14.885000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added info with G3522 & note in tib"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGF69B5087DC2D4B3F
        a                   adm:UpdateData ;
        adm:logDate         "2012-10-12T12:20:48.711000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added contain"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV1F768EEF879564B8
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1224"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P2624 .
    
    bdr:EV2C692E719D08E606
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:eventWhen       "1224"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3521  a            bdo:Place ;
        bf:identifiedBy     bdr:IDAF7560433FE7AFBD ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:note            bdr:NT0181A5300F12C907 , bdr:NT374CB4A019E52AE6 , bdr:NTAC1825BFE1747BAA , bdr:NTFFA1E063A3307940 ;
        bdo:placeEvent      bdr:EV1F768EEF879564B8 , bdr:EV2C692E719D08E606 ;
        bdo:placeGonpaPerEcumen  "12.25" ;
        bdo:placeLat        29.19799 ;
        bdo:placeLocatedIn  bdr:G799 ;
        bdo:placeLong       91.30387 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "gra nang rgyal gling tshogs pa/"@bo-x-ewts , "gyang gling tshogs pa/"@bo-x-ewts , "Gyaling Chokpa"@bo-x-phon-en , "jielin cuoba si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "rgyal gling tshogs pa/"@bo-x-ewts , "结林措巴寺"@zh-hans .
    
    bdr:IDAF7560433FE7AFBD
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3521" .
    
    bdr:NT0181A5300F12C907
        a                   bdo:Note ;
        bdo:noteText        "added from Chan's Guide"@en .
    
    bdr:NT374CB4A019E52AE6
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 159-160" ;
        bdo:noteSource      bdr:MW27524 ;
        bdo:noteText        "gyang ging tshogs pa zhes thog pa sogs lo rgyus gsal/"@bo-x-ewts .
    
    bdr:NTAC1825BFE1747BAA
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 2285" ;
        bdo:noteSource      bdr:MW26372 ;
        bdo:noteText        "1224. kha che paN chen gyi dngos slob mkhan chen byang chub dpal gyis tshogs sde bzhi'i ya gyal gra nang rgyal gling tshogs pa 'dzugs par gnang/ dgon pa 'dir rga gar ka ling ka rgyal po'i gdung skal bcom ldan 'das kyi tshems che ba bzhi'i grags gcig nang rten du bzhugs"@en .
    
    bdr:NTFFA1E063A3307940
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 26-28" ;
        bdo:noteSource      bdr:MW23847 ;
        bdo:noteText        "རྒྱལ་གླིང་ཚོགས་པ་ནི་དགའ་ཚལ་སྦུག་༼G3522༽དང་འབྲེལ་ནས་ཆགས་ཡོད་དེ། སྤྱིར་གནས་དེ་ནི་རྒྱལ་བ་ཡར་རྒྱབ་པའི་ཕ་གཞིས་ཡིན་པས་རྒྱལ་གླིང་ཞེས་གྲགས་པ་དང་། རྒྱལ་གླིང་ཚོགས་པ་ནི་ཁ་ཆེ་པཎ་ཆེན་གྱིས་ཕྱག་བཏབ་པའི་ཚོགས་པ་སྡེ་བཞིའི་ཡ་གྱལ་དུ་གྲགས་ལ་ནང་རྟེན་ཡང་ཁ་ཆེ་པཎ་ཆེན་གྱི་རྫ་སྐུ་སོགས་བཞུགས། དེ་དང་ཐག་ཉེར་དགའ་ཚལ་སྦུག་ཆགས་ཡོད་པར་འཁོད།"@bo .
}

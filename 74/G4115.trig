@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G4115 {
    bda:G4115  a            adm:AdminData ;
        adm:adminAbout      bdr:G4115 ;
        adm:facetIndex      11 ;
        adm:gitPath         "74/G4115.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G4115 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LGA34BAA261F8BF493 , bda:LGA960904FF718D23F , bda:LGD260773370177B69 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "513326210:470" ;
        adm:place_id_lex    "513326210:470" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGA34BAA261F8BF493
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-22T09:38:49.097000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGA960904FF718D23F
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-01T09:12:50.870000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGD260773370177B69
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV0BBDEEAD7173F7F3
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:eventWhen       "1850"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV5FF15B9ADD76D55F
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionKagyu .
    
    bdr:EVDEB28D0162F9E57C
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "17XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P7746 .
    
    bdr:G4115  a            bdo:Place ;
        bf:identifiedBy     bdr:ID39CD3AFDFD59251B ;
        bdo:associatedTradition  bdr:TraditionKagyu , bdr:TraditionNyingma ;
        bdo:note            bdr:NT1968E355FF3BCE77 ;
        bdo:placeEvent      bdr:EV0BBDEEAD7173F7F3 , bdr:EV5FF15B9ADD76D55F , bdr:EVDEB28D0162F9E57C ;
        bdo:placeGonpaPerEcumen  "4.14" ;
        bdo:placeLat        30.75000 ;
        bdo:placeLocatedIn  bdr:G2298 ;
        bdo:placeLong       101.16666 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "rnam dkar chos spyod gling /"@bo-x-ewts , "smug rong dkar chos dgon rnam dkar chos spyod gling /"@bo-x-ewts , "gaqia si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dkar chos dgon/"@bo-x-ewts , "呷卡寺"@zh-hans .
    
    bdr:ID39CD3AFDFD59251B
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G4115" .
    
    bdr:NT1968E355FF3BCE77
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 2, pp. 614-617" ;
        bdo:noteSource      bdr:MW19997 ;
        bdo:noteText        "rnying ma monastery in rta'u rdzong"@en .
}

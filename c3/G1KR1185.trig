@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1KR1185 {
    bda:G1KR1185  a         adm:AdminData ;
        adm:adminAbout      bdr:G1KR1185 ;
        adm:facetIndex      11 ;
        adm:gitPath         "c3/G1KR1185.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1KR1185 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG2F24093B05453F1A , bda:LGD24BFBC99FF7BFD9 , bda:LGDC2A59BB1F068A11 , bda:LGIM5E940A373A , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542125209:266" ;
        adm:place_id_lex    "542125209:266" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG2F24093B05453F1A
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-16T12:27:08.272000+00:00"^^xsd:dateTime ;
        adm:logMessage      "update list of names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGD24BFBC99FF7BFD9
        a                   adm:UpdateData ;
        adm:logDate         "2013-09-25T16:11:29.435000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & coord & event & notes"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGDC2A59BB1F068A11
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-16T12:14:48.550000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added tibetan name & event & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM5E940A373A
        a                   adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created by monastery import"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EV69793315086F8535
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1928"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P9306 .
    
    bdr:EVB055AFA3EAB306F4
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:eventWhen       "1928"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G1KR1185  a         bdo:Place ;
        bf:identifiedBy     bdr:ID438E4CA86ADC3C13 ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:note            bdr:NT10F11E82CCCC9A90 , bdr:NT88A85BEBFE2548E3 ;
        bdo:placeEvent      bdr:EV69793315086F8535 , bdr:EVB055AFA3EAB306F4 ;
        bdo:placeGonpaPerEcumen  "4.57" ;
        bdo:placeLat        31.776732 ;
        bdo:placeLocatedIn  bdr:G2188 ;
        bdo:placeLong       95.037260 ;
        bdo:placeType       bdr:PT0050 ;
        skos:altLabel       "卡堋寺"@zh-hans , "kabeng si"@zh-latn-pinyin-x-ndia , "kafeng si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "kha spungs dgon/"@bo-x-ewts , "卡崩寺"@zh-hans .
    
    bdr:ID438E4CA86ADC3C13
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1KR1185" .
    
    bdr:NT10F11E82CCCC9A90
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 181-182" ;
        bdo:noteSource      bdr:MW25095 ;
        bdo:noteText        "དགོན་འདི་ག་ངད་ཤང་ཁ་སྤུངས་གྲོང་ཚོར་གནས། ༡༩༢༨ལོར་ཚུལ་ཁྲིམས་དབང་མོས་ཕྱག་བཏབ།"@bo .
    
    bdr:NT88A85BEBFE2548E3
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 301" ;
        bdo:noteSource      bdr:MW25095 ;
        bdo:noteText        "bon po religious establishment founded in 1928 by tshul khrims dbang mo.\n\n20 nuns"@en .
}

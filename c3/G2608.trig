@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G2608 {
    bda:G2608  a            adm:AdminData ;
        adm:adminAbout      bdr:G2608 ;
        adm:facetIndex      12 ;
        adm:gitPath         "c3/G2608.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G2608 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG68AA37A910781AC8 , bda:LGABB1339620C40566 , bda:LGE167A4F21DC57086 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632725350:117" ;
        adm:place_id_lex    "632725350:117" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG68AA37A910781AC8
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-06T16:05:04.847000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location & arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGABB1339620C40566
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGE167A4F21DC57086
        a                   adm:UpdateData ;
        adm:logDate         "2010-10-27T12:54:03.190000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00002 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV2D6AC24347F4BC30
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1657"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P2032 .
    
    bdr:EVD45012C0BB5F77D7
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionKarmaKagyu ;
        bdo:eventWhen       "1511"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G2608  a            bdo:Place ;
        bf:identifiedBy     bdr:ID35255078B49975FF ;
        bdo:associatedTradition  bdr:TraditionKarmaKagyu ;
        bdo:note            bdr:NT0D82EA4A2782B85A , bdr:NT64CC72C33DDF8FF9 , bdr:NTF7EAAA5E8E495CE9 ;
        bdo:placeEvent      bdr:EV2D6AC24347F4BC30 , bdr:EVD45012C0BB5F77D7 ;
        bdo:placeGonpaPerEcumen  "7.03" ;
        bdo:placeLat        32.27601 ;
        bdo:placeLocatedIn  bdr:G1467 ;
        bdo:placeLong       96.57117 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "go che dge 'phel chos gling /"@bo-x-ewts , "go che dgon bshad grwa legs bshad dge ba'i gling /"@bo-x-ewts , "果千寺"@zh-hans , "顾且寺"@zh-hans , "guoqian si"@zh-latn-pinyin-x-ndia , "guqie si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "go che dgon/"@bo-x-ewts , "Goche Gon Gephel Choling"@en , "郭欠寺"@zh-hans .
    
    bdr:ID35255078B49975FF
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G2608" .
    
    bdr:NT0D82EA4A2782B85A
        a                   bdo:Note ;
        bdo:noteText        "first as a tent temple, used to be Nendo"@en .
    
    bdr:NT64CC72C33DDF8FF9
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, p. 46" ;
        bdo:noteSource      bdr:MW18134 ;
        bdo:noteText        "karma bka' brgyud pa monastery in yus hru'u prefecture, qinghai province"@en .
    
    bdr:NTF7EAAA5E8E495CE9
        a                   bdo:Note ;
        bdo:contentLocationStatement  "french flaps" ;
        bdo:noteSource      bdr:MW29647 ;
        bdo:noteText        "go che dge 'phel chos gling is located in the northern part of nang chen rdzong: nang chen rdzong gi byang phyogs le bar bzhi bcu tsam gyi sar chags\nfounded in 1657 by one of the four chief disciples of karma chags med: gnas mdo o rgyan mthar phyin\nhas established a publishing series go che dgon rig gnas dpe tshogs"@en .
}

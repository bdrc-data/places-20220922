@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1644 {
    bda:G1644  a            adm:AdminData ;
        adm:adminAbout      bdr:G1644 ;
        adm:facetIndex      12 ;
        adm:gitPath         "05/G1644.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1644 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG42F2622086D1BBC0 , bda:LG5E972BB8E8E5182E , bda:LGCD68DBA7DAE3C0C7 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "513330203:134" ;
        adm:place_id_lex    "513330203:134" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG42F2622086D1BBC0
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG5E972BB8E8E5182E
        a                   adm:UpdateData ;
        adm:logDate         "2010-08-11T17:36:02.159000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added note ffrom khams sde dge rgyal po'i srid don lo rgyus"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGCD68DBA7DAE3C0C7
        a                   adm:UpdateData ;
        adm:logDate         "2013-06-20T10:29:24.632000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & events & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV7C4382B613121666
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionSakya ;
        bdo:eventWhen       "12XX/13XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV80A3EA546C3DB744
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "12XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P2CN11094 .
    
    bdr:EVDEBAF1CD8B4E7986
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionDrigungKagyu ;
        bdo:eventWhen       "1225"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G1644  a            bdo:Place ;
        bf:identifiedBy     bdr:IDFDF6E7BE344AA2D8 ;
        bdo:associatedTradition  bdr:TraditionDrigungKagyu , bdr:TraditionSakya ;
        bdo:note            bdr:NT42F636180172B7D0 , bdr:NTFCE786BDE6EF4A35 ;
        bdo:placeEvent      bdr:EV7C4382B613121666 , bdr:EV80A3EA546C3DB744 , bdr:EVDEBAF1CD8B4E7986 ;
        bdo:placeGonpaPerEcumen  "8.26" ;
        bdo:placeLat        31.65000 ;
        bdo:placeLocatedIn  bdr:G1539 ;
        bdo:placeLong       98.78333 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "dpe dbar dgon/"@bo-x-ewts , "sde dge dpe war dgon/"@bo-x-ewts , "白垭寺"@zh-hans , "baiya si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dpe war dgon/"@bo-x-ewts , "白雅寺"@zh-hans .
    
    bdr:IDFDF6E7BE344AA2D8
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1644" .
    
    bdr:NT42F636180172B7D0
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, p. 466-471" ;
        bdo:noteSource      bdr:MW19997 ;
        bdo:noteText        "sa skya monastery\nའདིར་དགོན་འདིའི་ལོ་རྒྱུས་གནད་ཚང་ཞིག་བཞུགས།"@en .
    
    bdr:NTFCE786BDE6EF4A35
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p.123" ;
        bdo:noteSource      bdr:MW1KG5539 ;
        bdo:noteText        "sa skya monastery in sde dge territory founded in the 3rd ra byung (1147-1206) by dpal ldan byang chub gling pa, a student of 'bri gung 'jig rten gsum mgon; following the conflict between sa skya and 'bri gung known as the 'bri gung gling log, the monastery became sa skya.\nformerly there were more than 100 monks and lamas; now there are more than 50."@en .
}

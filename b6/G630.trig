@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G630 {
    bda:G630  a             adm:AdminData ;
        adm:adminAbout      bdr:G630 ;
        adm:facetIndex      8 ;
        adm:gitPath         "b6/G630.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G630 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG14F954D574812296 , bda:LG4A48A35953E610BE , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542328200:542328101" ;
        adm:place_id_lex    "542328200:542328101" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG14F954D574812296
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG4A48A35953E610BE
        a                   adm:UpdateData ;
        adm:logDate         "2012-06-29T16:57:10.428000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added name and location"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EVD036CA6884539E5E
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1400"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G630  a             bdo:Place ;
        bf:identifiedBy     bdr:IDAE826E08666A1B06 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT4C43DF6AEEB1FB76 ;
        bdo:placeEvent      bdr:EVD036CA6884539E5E ;
        bdo:placeGonpaPerEcumen  "3.55" ;
        bdo:placeLat        29.43333 ;
        bdo:placeLocatedIn  bdr:G2164 ;
        bdo:placeLong       88.20000 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "bzhad bkra shis dge 'phel/"@bo-x-ewts , "gtsang g.yas ru 'jad bkra shis dge 'phel/"@bo-x-ewts , "Tashi Gepel"@bo-x-phon-en , "Tashi Gepel"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "bzhad bkra shis dge 'phel/"@bo-x-ewts , "扎西吉培寺"@zh-hans .
    
    bdr:IDAE826E08666A1B06
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G630" .
    
    bdr:NT4C43DF6AEEB1FB76
        a                   bdo:Note ;
        bdo:noteText        "1300s a minor Kagyu nunnery, so just map as Geluk during Pakmodrupa period and later."@en .
}

@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3010 {
    bda:G3010  a            adm:AdminData ;
        adm:adminAbout      bdr:G3010 ;
        adm:facetIndex      11 ;
        adm:gitPath         "10/G3010.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3010 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG8E54398D30018EF1 , bda:LG8EE49F34207F3123 , bda:LG9BA1D0C4C99EF572 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542329206:542329103" ;
        adm:place_id_lex    "542329206:542329103" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8E54398D30018EF1
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG8EE49F34207F3123
        a                   adm:UpdateData ;
        adm:logDate         "2014-03-17T15:47:44.926000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalized names & added note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG9BA1D0C4C99EF572
        a                   adm:UpdateData ;
        adm:logDate         "2012-07-03T15:48:36.371000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV1354FA56BD34E9C6
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1412"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EV784DECD003C326A2
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "14XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P55 .
    
    bdr:G3010  a            bdo:Place ;
        bf:identifiedBy     bdr:ID862F8D86ABFA4557 ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT71C0247D510B654E , bdr:NT784E0D4E75D2F9DC ;
        bdo:placeEvent      bdr:EV1354FA56BD34E9C6 , bdr:EV784DECD003C326A2 ;
        bdo:placeGonpaPerEcumen  "4.26" ;
        bdo:placeLat        28.96667 ;
        bdo:placeLocatedIn  bdr:G2174 ;
        bdo:placeLong       89.18333 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "mdangs can dgon/"@bo-x-ewts , "ri bo mdangs chen dgon/"@bo-x-ewts , "Dangchen Gon"@bo-x-phon-en , "达坚贡巴"@zh-hans , "dajian si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "ri bo mdangs can dgon/"@bo-x-ewts , "达坚寺"@zh-hans .
    
    bdr:ID862F8D86ABFA4557
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3010" .
    
    bdr:NT71C0247D510B654E
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 72" ;
        bdo:noteSource      bdr:MW20233 ;
        bdo:noteText        "monastery in gtsang where spyan snga ba blo gros rgyal mtshan met his teacher mkhas grub rje"@en .
    
    bdr:NT784E0D4E75D2F9DC
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 220" ;
        bdo:noteSource      bdr:MW19801 ;
        bdo:noteText        "མཁས་གྲུབ་དགེ་ལེགས་དཔལ་བཟང་གིས་ལྕང་ར་དགོན་གྱི་མཁན་པོ་གནང་ཐོག་རི་བོ་མདངས་ཆེན་དགོན་པ་ཕྱག་འདེབས་པའི་གཙོ་སྐྱོང་གནང་ཞེས་གསལ།"@bo .
}

@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3555 {
    bda:G3555  a            adm:AdminData ;
        adm:adminAbout      bdr:G3555 ;
        adm:facetIndex      11 ;
        adm:gitPath         "35/G3555.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3555 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG0EF50AB8DBC5A1D0 , bda:LG3115A56000FDF481 , bda:LG50A987B245BEBE8F , bda:LG572D31E5B0B1CA51 , bda:LGC5DCF5720E3A27DD , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542328201:542328103" ;
        adm:place_id_lex    "542328201:542328103" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0EF50AB8DBC5A1D0
        a                   adm:UpdateData ;
        adm:logDate         "2013-01-09T15:48:07.845000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG3115A56000FDF481
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-02T14:06:15.294000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG50A987B245BEBE8F
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG572D31E5B0B1CA51
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-08T15:11:00.921000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added type"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGC5DCF5720E3A27DD
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-02T13:50:29.687000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & event & note"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EV09C6F8700CD13E53
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:eventWhen       "1161"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVF8045D71812996FD
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:eventWhen       "1233"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3555  a            bdo:Place ;
        bf:identifiedBy     bdr:ID3B97CDBA6A914288 ;
        bdo:associatedTradition  bdr:TraditionBon ;
        bdo:note            bdr:NT88A4417860D082B1 ;
        bdo:placeEvent      bdr:EV09C6F8700CD13E53 , bdr:EVF8045D71812996FD ;
        bdo:placeGonpaPerEcumen  "11.11" ;
        bdo:placeLat        29.41667 ;
        bdo:placeLocatedIn  bdr:G2164 ;
        bdo:placeLong       88.28333 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "bzhad dpal ldan dar lding gser sgo khra mo/"@bo-x-ewts , "dar ldings gser sgo khra mo/"@bo-x-ewts , "dar sdings gser sgo khra mo/"@bo-x-ewts , "seguo chamu si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dar sding gser sgo khra mo/"@bo-x-ewts , "Darding Sergo Tramo"@en , "色果查姆寺"@zh-hans .
    
    bdr:ID3B97CDBA6A914288
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3555" .
    
    bdr:NT88A4417860D082B1
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 259-260" ;
        bdo:noteSource      bdr:MW25095 ;
        bdo:noteText        "གཤེན་ཚང་གི་བོན་དགོན་འདི་ནི་༡༢༣༣ལོར་གཤེན་ཀུན་མཁྱེན་ཡེ་ཤེས་བློ་གྲོས་ཀྱིས་ཕྱག་བཏབ། སོག་པོ་ཇུན་གར་མཐར་བསྐྲད་བཏང་བའི་ལོ་རྒྱུས་ཡོད། བར་སྐབས་ཉམས་ཡང་ད་ལྟ་སོར་ཆུད། མཐོང་སྨོན་རྫོང་གི་བྱང་ངོས་སྤྱི་ལེ་གསུམ་ཙམ་ན་གནས་པ་སོགས་གསལ།"@bo .
}

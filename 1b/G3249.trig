@prefix :      <http://purl.bdrc.io/ontology/core/> .
@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3249 {
    bda:G3249  a            adm:AdminData ;
        adm:adminAbout      bdr:G3249 ;
        adm:facetIndex      10 ;
        adm:gitPath         "1b/G3249.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3249 ;
        adm:logEntry        bda:LG046CD0A57D719766 , bda:LG0BDSGLPZU0FYWSHW , bda:LG52AF28AA03E27057 , bda:LG6E777731DA3614D4 , bda:LGA51B2BEA3C094788 , bda:LGC356778E11C9D42A , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bda:LG046CD0A57D719766
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDSGLPZU0FYWSHW
        a                   adm:UpdateData ;
        adm:logAgent        "domm-import/domm-import.py" ;
        adm:logDate         "2024-06-26T09:00:04.708062"^^xsd:dateTime ;
        adm:logMessage      "import data from DOMM"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG52AF28AA03E27057
        a                   adm:UpdateData ;
        adm:logDate         "2022-05-20T10:57:56.749000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added other name and note from G00AG02558"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG6E777731DA3614D4
        a                   adm:UpdateData ;
        adm:logDate         "2022-05-24T13:53:26.806000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added name and date from G1979"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGA51B2BEA3C094788
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-13T14:13:51.543000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added ri bo dge rgyas dga' ldan bshad sgrub gling gi par khang"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGC356778E11C9D42A
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-21T16:08:57.266000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location & arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV7B6718A09E9C9D3D
        a                   :PlaceFounded ;
        :eventWhen          "1778"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3249  a            :Place ;
        rdfs:seeAlso        "https://mongoliantemples.org/mn/component/domm/1044?view=oldtempleen"^^xsd:anyURI , "https://mongoliantemples.org/mn/component/domm/23?view=oldtemplemn"^^xsd:anyURI ;
        bf:identifiedBy     bdr:IDG3249_001 ;
        :note               bdr:NTDB877A0B0000DEF9 , bdr:NTE62EFB53EF78D7FC , bdr:NTG3249_FD , bdr:NTG3249_P ;
        :placeAccuracy      "GIS coordinates have been truncated at the request of DOMM in order to preserve potential remains of the monastery."@en ;
        :placeEvent         bdr:EV7B6718A09E9C9D3D ;
        :placeLat           "47.92" ;
        :placeLocatedIn     bdr:G1395 , bdr:G2CN11044 , bdr:G3JT12593 , bdr:G584 ;
        :placeLong          "106.92" ;
        :placeType          bdr:PT0037 ;
        skos:altLabel       "dga' ldan bshad sgrub gling /"@bo-x-ewts , "hal ha'i yul gyi ri bo dge rgyas gling /"@bo-x-ewts , "hu re chen mo/"@bo-x-ewts , "khu ral chen mo dga' ldan bshad sgrub gling /"@bo-x-ewts , "khu re chen mo/"@bo-x-ewts , "khu ril chen mo/"@bo-x-ewts , "ri bo dge rgyas dga' ldan bshad sgrub gling /"@bo-x-ewts , "ri bo dge rgyas gling /"@bo-x-ewts , "tA hu re/"@bo-x-ewts , "tA khu ral/"@bo-x-ewts , "ta khu re dgon chen/"@bo-x-ewts , "tshogs chen 'du khang /"@bo-x-ewts , "u rge ri bo dge rgyas dga' ldan bshad sgrub gling /"@bo-x-ewts , "Eregbogejigandanshaddublin"@mn-Latn , "Ölziig badruulagch tögs bayasgalant nomlol büteeliin süm"@mn-Latn , "Rebogejai Gandanshaddublin"@mn-Latn , "Rebogeji Gandanshadüwlin"@mn-Latn ;
        skos:prefLabel      "ri bo dge rgyas dgon/"@bo-x-ewts , "Takure Gonchen"@en , "Nomiin Ikh Khüree"@mn-Latn .
    
    bdr:IDG3249_001  a      bdr:DommId ;
        rdf:value           "UBR 910" .
    
    bdr:NTDB877A0B0000DEF9
        a                   :Note ;
        :noteText           "A Gelug monastery in Mongolia, hal ha'i yul; according to mkhyen brtse (gdan rabs p. 484) it was the largest monastery in Mongolia. It was founded by blo bzang bstan rgyas, the incarnation of tA ra nA tha."@en .
    
    bdr:NTE62EFB53EF78D7FC
        a                   :Note ;
        :noteText           "A monastery located in the capital of Mongolia, Ulanbator (Urga)\nThere was a great maitreya image built and consecrated in 1833 by the 5th rje btsun dam pa blo bzang tshul khrims 'jigs med bstan pa'i rgyal mtshan."@en .
    
    bdr:NTG3249_FD  a       :Note ;
        :noteText           "Foundation date on DOMM: The capital relocated to the overall area of today,moved several times within this territory before s"@en .
    
    bdr:NTG3249_P  a        :Note ;
        :noteText           "This record has been updated with data from the “Documentation of Mongolian Monasteries” project with due authorization. For more information on this monastery, including oral history, historical records and pictures, see the links above."@en .
}

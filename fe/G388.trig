@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G388 {
    bda:G388  a             adm:AdminData ;
        adm:adminAbout      bdr:G388 ;
        adm:facetIndex      12 ;
        adm:gitPath         "fe/G388.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G388 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG46572F8A985451EE , bda:LG4DE9440E314E7711 , bda:LG87E239375482DA76 , bda:LG88E34D7C0C373A2C , bda:LGA2468EE9D4EB0585 , bda:LGIM61A98BA6 , bda:LGIM7555E0C976 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542228200:542228110" ;
        adm:place_id_lex    "542228200:542228110" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG46572F8A985451EE
        a                   adm:UpdateData ;
        adm:logDate         "2007-02-07T12:09:22.460000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added name"@en ;
        adm:logWho          bdu:U00002 .
    
    bda:LG4DE9440E314E7711
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-12T14:17:29.027000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG87E239375482DA76
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG88E34D7C0C373A2C
        a                   adm:UpdateData ;
        adm:logDate         "2012-06-08T15:22:04.773000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location & GIS ID"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGA2468EE9D4EB0585
        a                   adm:UpdateData ;
        adm:logDate         "2013-11-27T12:41:16.125000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalized"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM7555E0C976
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:29:31.108000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from temple to lhaKhang"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EVDC7C49B32A129F91
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:eventWhen       "1250"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G388  a             bdo:Place ;
        bf:identifiedBy     bdr:ID0D1BC5F6E6D15ED5 ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:note            bdr:NT616D2C8156FB45CE , bdr:NT9339BA4C4C20112B ;
        bdo:placeEvent      bdr:EVDC7C49B32A129F91 ;
        bdo:placeGonpaPerEcumen  "1.94" ;
        bdo:placeLat        28.36870 ;
        bdo:placeLocatedIn  bdr:G2134 ;
        bdo:placeLong       90.66302 ;
        bdo:placeType       bdr:PT0074 ;
        skos:altLabel       "bsam grub bde ba chen po/"@bo-x-ewts , "gu ru lha khang /"@bo-x-ewts , "Layak Guru Lhakhang"@bo-x-phon-en , "laya guru lakang"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "la yag gu ru lha khang /"@bo-x-ewts , "拉雅古如拉康"@zh-hans .
    
    bdr:ID0D1BC5F6E6D15ED5
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G388" .
    
    bdr:NT616D2C8156FB45CE
        a                   bdo:Note ;
        bdo:noteText        "rebuilt in 1949"@en .
    
    bdr:NT9339BA4C4C20112B
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 253" ;
        bdo:noteSource      bdr:MW21611 ;
        bdo:noteText        "description\nimportant rnying ma seat of the gter ma tradition of gu ru chos kyi dbang phyug"@en .
}

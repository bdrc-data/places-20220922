@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3493 {
    bda:G3493  a            adm:AdminData ;
        adm:adminAbout      bdr:G3493 ;
        adm:facetIndex      15 ;
        adm:gitPath         "75/G3493.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3493 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG3926F38B4BF53C91 , bda:LG564507A62CAF85D7 , bda:LG9A8B59EDAE3795EB , bda:LGA144D13A2C8FC2A7 , bda:LGAFD8E32A40E0F598 , bda:LGB4C98D31D5039EA1 , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "513321100:96" ;
        adm:place_id_lex    "513321100:96" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3926F38B4BF53C91
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG564507A62CAF85D7
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-02T14:42:13.798000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added events"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG9A8B59EDAE3795EB
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-03T11:24:42.896000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged nmaes"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGA144D13A2C8FC2A7
        a                   adm:UpdateData ;
        adm:logDate         "2014-06-17T16:37:39.396000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalized"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGAFD8E32A40E0F598
        a                   adm:UpdateData ;
        adm:logDate         "2007-02-06T00:31:29.093000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00001 .
    
    bda:LGB4C98D31D5039EA1
        a                   adm:UpdateData ;
        adm:logDate         "2010-08-15T16:31:05.950000+00:00"^^xsd:dateTime ;
        adm:logMessage      "note added from khams sde dge rgyal po'i srid don lo rgyus"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV8F7E71DF4816A5AC
        a                   bdo:PlaceFounded ;
        bdo:eventWhen       "1564"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWho        bdr:P646 .
    
    bdr:EVB26712815DB21CA1
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:eventWhen       "1564"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3493  a            bdo:Place ;
        bf:identifiedBy     bdr:ID4D18CEC7B95DA7C6 ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:note            bdr:NT2002F69285E58503 , bdr:NT618302083A532EAF , bdr:NTEB8F1EB6312590EA ;
        bdo:placeEvent      bdr:EV8F7E71DF4816A5AC , bdr:EVB26712815DB21CA1 ;
        bdo:placeGonpaPerEcumen  "22.36" ;
        bdo:placeLat        30.05000 ;
        bdo:placeLocatedIn  bdr:G2308 ;
        bdo:placeLong       101.96667 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "rdor brag dgon (smad)"@bo-x-ewts , "rdo rje brag dgon/"@bo-x-ewts , "康定金刚寺"@zh-hans , "jingang si"@zh-latn-pinyin-x-ndia , "kangding jingang si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dar mdo rdo rje brag dgon/"@bo-x-ewts , "金刚寺"@zh-hans .
    
    bdr:ID4D18CEC7B95DA7C6
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3493" .
    
    bdr:NT2002F69285E58503
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 129" ;
        bdo:noteSource      bdr:MW1KG5539 ;
        bdo:noteText        "rnying ma monastery in dar rtse mdo rdzong; this source is incorrect in stating that the monastery was founded 1126 (rab byung 2 me rta) by the rdor brag rig 'dzin chen po skal bzang theg mchog bstan pa'i rgyal mtshan (this should be bskal bzang pad+ma dbang phyug (1719/1720-1770). it is likely that the date of founding is in middle of the 18th century. \n\nformerly for religious ceremonies there were 180+ monks; now there are 40 monks."@en .
    
    bdr:NT618302083A532EAF
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 3, pp. 16-19" ;
        bdo:noteSource      bdr:MW19997 ;
        bdo:noteText        "recently restored rnying ma monastery of the byang gter tradition;\nthis was the family monastery of the lcags la rulers of dar rtse mdo;\nrecently restored\n\nའདི་ནི་རྙིང་མའི་གདན་ས་ཆེན་པོ་ཐུབ་བསྟན་རྡོ་རྗེ་བྲག་གི་བུ་དགོན་ཆེ་གྲས་ཤིག་ཡིན།"@en .
    
    bdr:NTEB8F1EB6312590EA
        a                   bdo:Note ;
        bdo:noteText        "pinyin: jin gang si; nyingma monastery founded in 1564 in sheng jiao xiang, dar mdo rdzong"@en .
}

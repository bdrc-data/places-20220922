@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G4462 {
    bda:G4462  a            adm:AdminData ;
        adm:adminAbout      bdr:G4462 ;
        adm:facetIndex      8 ;
        adm:gitPath         "27/G4462.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G4462 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG62F53B432E0B056A , bda:LGB6BF4919EAC16CB9 , bda:LGIM61A98BA6 , bda:LGIM7555E0C976 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542221204:542221114" ;
        adm:place_id_lex    "542221204:542221114" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG62F53B432E0B056A
        a                   adm:UpdateData ;
        adm:logDate         "2012-05-18T16:17:57.570000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added GIS ID & note in tib"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGB6BF4919EAC16CB9
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM7555E0C976
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:29:31.108000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from temple to lhaKhang"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV6D7877C1B9BCFD24
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionRime ;
        bdo:eventWhen       "0750"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G4462  a            bdo:Place ;
        bf:identifiedBy     bdr:IDABCDAE2E0064978F ;
        bdo:associatedTradition  bdr:TraditionRime ;
        bdo:note            bdr:NTCBACB47227C30045 ;
        bdo:placeEvent      bdr:EV6D7877C1B9BCFD24 ;
        bdo:placeGonpaPerEcumen  "11.4" ;
        bdo:placeLat        29.10854 ;
        bdo:placeLocatedIn  bdr:G1513 ;
        bdo:placeLong       91.81752 ;
        bdo:placeType       bdr:PT0074 ;
        skos:altLabel       "brgya bza' lha khang /"@bo-x-ewts , "rgya bza' khang /"@bo-x-ewts , "Gyazakhang"@bo-x-phon-en , "金城公主庙"@zh-hans , "Gyazakhang"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "rgya bza' khang /"@bo-x-ewts , "甲萨拉康"@zh-hans .
    
    bdr:IDABCDAE2E0064978F
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G4462" .
    
    bdr:NTCBACB47227C30045
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, pp. 53-54" ;
        bdo:noteSource      bdr:MW23847 ;
        bdo:noteText        "ལྷ་ཁང་དེ་ནི་ཡར་ལྷ་ཤམ་བུའི་ཆུ་ཐོན་ནས་སོང་ན་ཟེ་སྟོད་གྲོང་གི་སྟོད་དུ་ཆགས་ཡོད། གྱིམ་ཤིང་ཀོང་ཇོའི་བཞུགས་ས་ཡིན་པར་གྲགས། སྔོན་དེར་ཆོས་རྒྱལ་མེས་ཨག་ཚོམ་གྱི་ཕྱག་བཟོ་སྤྱན་རས་གཟིགས་ཕྱག་སྟོང་སྤྱན་སྟོང་གི་སྐུ་ཞིག་ཡོད་པ་དེ་ཉམས་ཆགས་བྱུང་བར། དུས་ཕྱིས་ས་དེ་རང་གིས་བཞེངས་པའི་སྤྱན་རས་གཟིགས་ཕྱག་སྟོང་སྤྱན་སྟོང་གི་སྐུ་བརྙན་ཞིག་ལྷ་ཁང་རྙིང་པའི་ནང་དུ་མཇལ་རྒྱུ་ཡོད་པ་སོགས་འཁོད།"@bo .
}

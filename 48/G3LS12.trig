@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G3LS12 {
    bda:G3LS12  a           adm:AdminData ;
        adm:adminAbout      bdr:G3LS12 ;
        adm:facetIndex      10 ;
        adm:gitPath         "48/G3LS12.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G3LS12 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG17C4B88B95E5E8ED , bda:LG91A1983A8A8115E9 , bda:LGBE55426EA6B61D38 , bda:LGC7E52413A4814C9F , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542524100:73" ;
        adm:place_id_lex    "542524100:73" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG17C4B88B95E5E8ED
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-28T15:14:01.375000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & location"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG91A1983A8A8115E9
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:19:35.197000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from placeOfPilgrimage to gnas"@en ;
        adm:logWho          bdu:U00020 .
    
    bda:LGBE55426EA6B61D38
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-17T11:35:59.027000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGC7E52413A4814C9F
        a                   adm:WithdrawData ;
        adm:logDate         "2010-03-31T15:25:07.921000+00:00"^^xsd:dateTime ;
        adm:logMessage      "withdrawn"@en ;
        adm:logWho          bdu:U00019 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EV2A02447D8156146C
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk .
    
    bdr:EVEBA270895CC7A207
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionKagyu ;
        bdo:eventWhen       "10XX"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G3LS12  a           bdo:Place ;
        bf:identifiedBy     bdr:ID36374D2B272AC156 ;
        bdo:associatedTradition  bdr:TraditionGeluk , bdr:TraditionKagyu ;
        bdo:note            bdr:NT29782F99C9103D0D ;
        bdo:placeEvent      bdr:EV2A02447D8156146C , bdr:EVEBA270895CC7A207 ;
        bdo:placeGonpaPerEcumen  "0.26" ;
        bdo:placeLat        33.82000 ;
        bdo:placeLocatedIn  bdr:G2156 , bdr:G3LS14 ;
        bdo:placeLong       79.86000 ;
        bdo:placeType       bdr:PT0050 ;
        skos:altLabel       "dbod byang dgon/"@bo-x-ewts , "Wojang Jomo Gon"@bo-x-phon-en , "weiqiang juemupu si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dbod byang jo mo dgon/"@bo-x-ewts , "卫强觉母普寺"@zh-hans .
    
    bdr:ID36374D2B272AC156
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G3LS12" .
    
    bdr:NT29782F99C9103D0D
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 4, p. 110" ;
        bdo:noteSource      bdr:MW23847 ;
        bdo:noteText        "Located to the north of od. Many ruins of hermitage sites of seven nuns and also ruins of castle"@en .
}

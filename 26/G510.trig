@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G510 {
    bda:G510  a             adm:AdminData ;
        adm:adminAbout      bdr:G510 ;
        adm:facetIndex      11 ;
        adm:gitPath         "26/G510.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G510 ;
        adm:logEntry        bda:LG01AF214018A06FA2 , bda:LG0BDSG2MOZVR13LOA , bda:LGA5D50B29C945D0A9 , bda:LGC498909835308502 , bda:LGDC3AD037C5790B4C , bda:LGIM61A98BA6 , bda:LGIM8BCFDFCA43 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "542233101:542233101" ;
        adm:place_id_lex    "542233101:542233101" ;
        adm:status          bda:StatusReleased .
    
    bda:LG01AF214018A06FA2
        a                   adm:UpdateData ;
        adm:logDate         "2012-06-08T15:21:25.803000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location & GIS ID"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGA5D50B29C945D0A9
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGC498909835308502
        a                   adm:UpdateData ;
        adm:logDate         "2013-11-21T12:42:02.501000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalized & added note from G3610(dup.)"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGDC3AD037C5790B4C
        a                   adm:UpdateData ;
        adm:logDate         "2014-02-19T14:32:27.181000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added location"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM8BCFDFCA43
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from monastery to dgonPa"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00020 .
    
    bdr:EV7B300DD2310AB8BF
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:eventWhen       "1350"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G510  a             bdo:Place ;
        bf:identifiedBy     bdr:ID70760E881DA77BE9 ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:note            bdr:NT674FCEE5DA94823D , bdr:NT841F8EB531DAEF6A ;
        bdo:placeEvent      bdr:EV7B300DD2310AB8BF ;
        bdo:placeGonpaPerEcumen  "2.21" ;
        bdo:placeLat        28.75301 ;
        bdo:placeLocatedIn  bdr:G2136 , bdr:G4683 ;
        bdo:placeLong       90.48135 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "brag ra bsam gtan chos gling /"@bo-x-ewts , "yar 'brog brag ra/"@bo-x-ewts , "Dragra Gonpa"@bo-x-phon-en , "zhare si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "brag ra dgon/"@bo-x-ewts , "扎热寺"@zh-hans .
    
    bdr:ID70760E881DA77BE9
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G510" .
    
    bdr:NT674FCEE5DA94823D
        a                   bdo:Note ;
        bdo:noteText        "rnying ma monastery in the yar 'brog area"@en .
    
    bdr:NT841F8EB531DAEF6A
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, pp. 137-139" ;
        bdo:noteSource      bdr:MW23847 ;
        bdo:noteText        "bka' brgyud pa monastery recently restored, now rnying ma pa\n\n---moved from G3610(dup.)"@en .
}

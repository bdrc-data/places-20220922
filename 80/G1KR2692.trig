@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G1KR2692 {
    bda:G1KR2692  a         adm:AdminData ;
        adm:adminAbout      bdr:G1KR2692 ;
        adm:facetIndex      11 ;
        adm:gitPath         "80/G1KR2692.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G1KR2692 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG56A3272D13D2DE03 , bda:LG874B6220792764BF , bda:LG9063778763D74B26 , bda:LGCB0D7BEEA0D1F437 , bda:LGIM5E940A373A , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632721200:23" ;
        adm:place_id_lex    "632721200:23" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG56A3272D13D2DE03
        a                   adm:UpdateData ;
        adm:logDate         "2013-06-24T11:55:50.596000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added info from G450"@en ;
        adm:logWho          bdu:U00020 .
    
    bda:LG874B6220792764BF
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-30T15:55:20.287000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added names & pinyin"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG9063778763D74B26
        a                   adm:UpdateData ;
        adm:logDate         "2022-06-01T11:38:57.975000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added rong tha rwa nyag theg chen gling"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGCB0D7BEEA0D1F437
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-20T12:18:32.406000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added short name"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM5E940A373A
        a                   adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created by monastery import"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EVA75B5B7C90E47DCB
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionDrigungKagyu ;
        bdo:eventWhen       "1295"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVBFF0B65F464469F6
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "Wanlimiddle"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G1KR2692  a         bdo:Place ;
        bf:identifiedBy     bdr:ID5D363B0EE5A968F8 ;
        bdo:associatedTradition  bdr:TraditionDrigungKagyu , bdr:TraditionGeluk ;
        bdo:note            bdr:NT48EFCDAC60C945A3 ;
        bdo:placeEvent      bdr:EVA75B5B7C90E47DCB , bdr:EVBFF0B65F464469F6 ;
        bdo:placeGonpaPerEcumen  "8.79" ;
        bdo:placeLat        33.15463 ;
        bdo:placeLocatedIn  bdr:G2326 ;
        bdo:placeLong       96.92091 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "rag nyag dgon phun tshogs theg chen gling /"@bo-x-ewts , "rong tha rwa nyag theg chen gling /"@bo-x-ewts , "rwa nyag phun tshogs theg chen gling /"@bo-x-ewts , "热尼牙寺"@zh-hans , "rangniang si"@zh-latn-pinyin-x-ndia , "reniya si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "rag nyag dgon/"@bo-x-ewts , "让娘寺"@zh-hans .
    
    bdr:ID5D363B0EE5A968F8
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G1KR2692" .
    
    bdr:NT48EFCDAC60C945A3
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, p. 47a" ;
        bdo:noteSource      bdr:MW18134 ;
        bdo:noteText        "dge lugs pa monastery in yus hru'u prefecture, qinghai province; seat of the rwa nyag incarnation lineage"@en .
}

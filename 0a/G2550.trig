@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:G2550 {
    bda:G2550  a            adm:AdminData ;
        adm:adminAbout      bdr:G2550 ;
        adm:facetIndex      11 ;
        adm:gitPath         "0a/G2550.trig" ;
        adm:gitRepo         bda:GR0005 ;
        adm:graphId         bdg:G2550 ;
        adm:logEntry        bda:LG0BDSG2MOZVR13LOA , bda:LG8875CEE6F186AABF , bda:LG911869A7F71BFC9A , bda:LGBDC3959F69CB9FAA , bda:LGDC856945D2764AED , bda:LGIM61A98BA6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:place_id_TBRC   "632721204:15" ;
        adm:place_id_lex    "632721204:15" ;
        adm:status          bda:StatusReleased .
    
    bda:LG0BDSG2MOZVR13LOA
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-06-25T15:39:40.235772"^^xsd:dateTime ;
        adm:logMessage      "add tradition"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8875CEE6F186AABF
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-05T11:48:24.635000+00:00"^^xsd:dateTime ;
        adm:logMessage      "arranged names"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG911869A7F71BFC9A
        a                   adm:UpdateData ;
        adm:logDate         "2012-12-11T17:11:51.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "type changed from placeTypes:monastery to dgonPa"@en ;
        adm:logWho          bdu:U00020 .
    
    bda:LGBDC3959F69CB9FAA
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGDC856945D2764AED
        a                   adm:UpdateData ;
        adm:logDate         "2012-11-19T16:18:49.528000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added place type"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LGIM61A98BA6  a     adm:UpdateData ;
        adm:logAgent        "monastery import" ;
        adm:logDate         "2012-12-25T01:09:21.507000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated place from monastery csv"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bdr:EV0C946C2F4575E4A9
        a                   bdo:PlaceFounded ;
        bdo:associatedTradition  bdr:TraditionDrigungKagyu ;
        bdo:eventWhen       "1394"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVE79A34BE1EA92027
        a                   bdo:PlaceConverted ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:eventWhen       "1750"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:G2550  a            bdo:Place ;
        bf:identifiedBy     bdr:ID98AFBF643C028E60 ;
        bdo:associatedTradition  bdr:TraditionDrigungKagyu , bdr:TraditionGeluk ;
        bdo:note            bdr:NT52A6CA3E03029F1A , bdr:NTC1797423C1CA320F ;
        bdo:placeEvent      bdr:EV0C946C2F4575E4A9 , bdr:EVE79A34BE1EA92027 ;
        bdo:placeGonpaPerEcumen  "3.76" ;
        bdo:placeLat        32.66369 ;
        bdo:placeLocatedIn  bdr:G2326 ;
        bdo:placeLong       96.60918 ;
        bdo:placeType       bdr:PT0037 ;
        skos:altLabel       "dga' ldan chos 'khor gling /"@bo-x-ewts , "klung shod dgon dga' ldan chos 'khor gling /"@bo-x-ewts , "rab shis klung shod dga' ldan chos 'khor gling /"@bo-x-ewts , "longxi si"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "klung shod dgon/"@bo-x-ewts , "龙喜寺"@zh-hans .
    
    bdr:ID98AFBF643C028E60
        a                   bdr:CHGISId ;
        rdf:value           "TBRC_G2550" .
    
    bdr:NT52A6CA3E03029F1A
        a                   bdo:Note ;
        bdo:noteText        "made Drigung Kagyu tent temple from Bonpo"@en .
    
    bdr:NTC1797423C1CA320F
        a                   bdo:Note ;
        bdo:contentLocationStatement  "v. 1, p. 163" ;
        bdo:noteSource      bdr:MW18134 ;
        bdo:noteText        "monastery in the yu hru'u prefecture of qinghai"@en .
}
